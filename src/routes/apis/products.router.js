const { Router } = require('express')
const ProductDaoMongo = require('../../daos/Mongo/productManagerMongo.js')

const router = Router()
const productsService = new ProductDaoMongo()

router.get('/', async (req, res)=>{
    try {
        const products = await productsService.getProducts()
        // console.log(products)
        res.send({
            status: 'success',
            payload: products
        })
        
    } catch (error) {
        console.log(error)
    }
})
router.post('/', async (req, res)=>{
    try {
        const newProduct = req.body
        if (!newProduct.title || !newProduct.stock ) {
            return res.status(400).send({status: 'error', error: 'Faltan mandar campos obligatorios'})
        }
        const result = await productsService.createProduct(newProduct)
        
        res.send({status: 'success', payload: result})
    } catch (error) {
        console.log(error)
    }
})
router.put('/', (req, res)=>{
    res.send('put productos')
})
router.delete('/', (req, res)=>{
    res.send('delete productos')
})

module.exports = router