exports.authorizationJwt = role => {
    return async (req, res, next) => {
        // console(req.use)
        if(!req.user) return res.status(401).send({error: 'Unauthorized'})
        // {id: 'alshjdfkjshda', role: 'admin'}
        if(req.user.role !== role) return res.status(401).send({error: 'Not permissions'})
        ///req.user = {user: {}, role}
        next()
    }
}