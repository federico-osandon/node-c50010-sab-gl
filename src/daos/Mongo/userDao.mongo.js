const { usersModel } = require("../../models/users.model")

class UserDaoMongo { // manager User
    constructor() {
        //  iniciar la base de datos
        this.userModel = usersModel
    }

    getUsersPaginate = async (limit=10, page=1)=> await this.userModel.paginate({},{limit, page, lean: true})
   
    getUsers         = async _ => await this.userModel.find({})
    
    geUsertBy        = async (filter) => await this.userModel.findOne(filter)
    
    createUser       = async (newUser)=> await this.userModel.create(newUser)
    
    updateUser       = async (uid, userUpdate) => await this.userModel.findOneAndUpdate({_id: uid}, userUpdate)

    deleteUser       = async (uid) => await this.userModel.findOneAndDelete({_id: uid})

}
    
module.exports = UserDaoMongo
    